// import openWindowIcon from '../assets/openwindow.svg'

function register ({ registerHook }) {
  registerHook({
    target: 'action:router.navigation-end',
    handler: async ({ path }) => {
      /* if (/^\/about/.test(path)) {
        await updateQueerMotionLink('my-about-instance .block.short-description')
      }

      if (/^\/signup$/.test(path)) {
        await updateQueerMotionLink('.instance-short-description')
      }

      if (/^\/login$/.test(path)) {
        await updateQueerMotionLink('.instance-short-description')
      } */
    }
  })
}
/*
async function updateQueerMotionLink (selector) {
  try {
    const shortDescription = await getElement(selector)
    const shortDescriptionText = await getTextContent(shortDescription)

    shortDescription.innerHTML = shortDescriptionText.replace('le collectif QueerMotion', `le <a href=https://collectif.queermotion.org target=_blank>collectif QueerMotion ${openWindowIcon}</a>`)
  } catch (error) {
    console.log(error)
  }
}

function getElement (selector) {
  // Waiting for DOMContent updated with a timeout of 5 seconds
  return new Promise((resolve, reject) => {
    const timeout = setTimeout(() => {
      clearInterval(interval)
      reject(new Error('DOMContent cannot be loaded'))
    }, 5000)

    // Waiting for selector in DOM
    const interval = setInterval(() => {
      if (document.querySelector(selector) !== null) {
        clearTimeout(timeout)
        clearInterval(interval)
        resolve(document.querySelector(selector))
      }
    }, 10)
  })
}

function getTextContent (element) {
  // Waiting for textContent with a timeout of 5 seconds
  return new Promise((resolve, reject) => {
    const timeout = setTimeout(() => {
      clearInterval(interval)
      reject(new Error('textContent cannot be loaded'))
    }, 5000)

    // Waiting for textContent
    const interval = setInterval(() => {
      if (element.textContent !== '') {
        clearTimeout(timeout)
        clearInterval(interval)
        resolve(element.textContent)
      }
    }, 10)
  })
} */

export {
  register
}
